import React, { Component } from "react";

class HotelRoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roomCount: 1,
      adultCount: 3,
      childCount: 0
    };
  }

  // ---------------------------------------- Room (+) ------------------------------------------------------------ //
  increseRoomCount = () => {
    const { roomCount, adultCount } = this.state;

    this.setState({
      roomCount: roomCount + 1,
      adultCount: adultCount > roomCount ? adultCount : roomCount + 1
    });
  };

  // ----------------------------------------Room (-) --------------------------------------------------------------- //
  decreaseRoomCount = () => {
    const { roomCount, adultCount } = this.state;

    this.setState({
      roomCount: roomCount - 1,
      childCount: 0,
      adultCount:
        (roomCount - 1) * 4 > adultCount ? adultCount : (roomCount - 1) * 4
    });
  };

  //  ------------------------------------------ Adult (+) -------------------------------------------------------- //
  increseAdultCount = () => {
    const { roomCount, adultCount, childCount } = this.state;

    this.setState({
      adultCount: adultCount + 1,
      childCount:
        childCount > 0 && adultCount + childCount === roomCount * 4
          ? childCount - 1
          : childCount
    });
  };

  // ------------------------------------------Adult (-)-------------------------------------------------------------- //
  decreaseAdultCount = () => {
    const { adultCount } = this.state;
    this.setState({ adultCount: adultCount - 1 });
  };

  // -----------------------------------------Children (+) ---------------------------------------------------------//
  increseChildrenCount = () => {
    const { childCount } = this.state;

    this.setState({ childCount: childCount + 1 });
  };

  // -----------------------------------------Children (-)--------------------------------------------------------- //
  decreaseChildrenCount = () => {
    const { childCount } = this.state;
    this.setState({ childCount: childCount - 1 });
  };

  // --------------------------------------------------Render--------------------------------------------------//
  render() {
    const { roomCount, adultCount, childCount } = this.state;
    const disabled = "disabled-button";
    const roomDcr = roomCount === 1 && disabled;
    const roomInc = roomCount === 5 && disabled;
    const adultDcr = adultCount === roomCount && disabled;
    const adultInc = roomCount * 4 === adultCount && disabled;
    const childDcr = childCount === 0 && disabled;
    const childInc = roomCount * 4 === adultCount + childCount && disabled;
    return (
      <div className="totalBlock">
        {/* ================================ ( HEADER )================================== */}

        <div className="headerContentDiv">
          <span style={{ color: "#293189" }}>
            <i className="fas fa-users" style={{ fontSize: "20px" }} />
            &nbsp; Choose number of <b>people</b>
          </span>
        </div>
        {/* ================================== ( ROOM Content ) ========================= */}

        <div className="content">
          <div className="rooms">
            <div className="row">
              <span className="col-lg-6 col-md-6 col-xl-6 labelContentDiv">
                <span className="labelContent">
                  <i className="fas fa-bed" />
                  &nbsp; ROOMS
                </span>
              </span>
              <span className="col-lg-6 col-md-6 col-xl-6 countContentDiv">
                <span className="countContent">
                  <i
                    className={`fas fa-minus-circle incDcrIcons dcrIcons ${roomDcr}`}
                    onClick={this.decreaseRoomCount}
                  />
                  {roomCount}
                  <i
                    className={`fas fa-plus-circle incDcrIcons incIcons ${roomInc}`}
                    onClick={this.increseRoomCount}
                  />
                </span>
              </span>
            </div>
          </div>
          <hr />
          {/* =================================( ADULT Content )============================= */}

          <div className="adults">
            <div className="row">
              <span className="col-lg-6 col-md-6 col-xl-6 labelContentDiv">
                <span className="labelContent">
                  <i className="fas fa-user" />
                  &nbsp; ADULTS
                </span>
              </span>
              <span className="col-lg-6 col-md-6 col-xl-6 countContentDiv">
                <span className="countContent">
                  <i
                    className={`fas fa-minus-circle incDcrIcons dcrIcons ${adultDcr}`}
                    onClick={this.decreaseAdultCount}
                  />
                  {adultCount}
                  <i
                    className={`fas fa-plus-circle  incDcrIcons incIcons ${adultInc}`}
                    onClick={this.increseAdultCount}
                  />
                </span>
              </span>
            </div>
          </div>
          <hr />
          {/* ======================================== ( CHILDREN Content ) ======================================= */}

          <div className="children">
            <div className="row">
              <span className="col-lg-6 col-md-6 col-xl-6 labelContentDiv">
                <span className="labelContent">
                  <i className="fas fa-child" />
                  &nbsp; CHILDREN
                </span>
              </span>
              <span className="col-lg-6 col-md-6 col-xl-6 countContentDiv">
                <span className="countContent">
                  <i
                    onClick={this.decreaseChildrenCount}
                    className={`fas fa-minus-circle incDcrIcons dcrIcons ${childDcr}`}
                  />
                  {childCount}
                  <i
                    className={`fas fa-plus-circle incDcrIcons incIcons ${childInc}`}
                    onClick={this.increseChildrenCount}
                  />
                </span>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default HotelRoom;
