import React, { Component } from "react";
import "./App.css";
import HotelRoom from "./HotelRooms";

class App extends Component {
  render() {
    return (
      <div className="container">
        <HotelRoom />
      </div>
    );
  }
}

export default App;
